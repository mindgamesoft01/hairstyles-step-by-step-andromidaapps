plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
}

android {
    namespace = "com.am.hair.app3.hairstylesstepbystep"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.am.hair.app3.hairstylesstepbystep"
        minSdk = 24
        targetSdk = 34
        versionCode = 6
        versionName = "1.0.5"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {

    //implementation fileTree (dir: 'libs', include: ['*.jar'])
    implementation ("androidx.multidex:multidex:2.0.1")
    implementation ("org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.9.10")
    implementation ("androidx.appcompat:appcompat:1.6.1")
    implementation ("com.google.android.material:material:1.10.0")
    implementation ("androidx.constraintlayout:constraintlayout:2.1.4")
    testImplementation ("junit:junit:4.13.2")
    androidTestImplementation ("androidx.test:runner:1.5.2")
    androidTestImplementation ("androidx.test.espresso:espresso-core:3.5.1")

    //def room_version = "1.0.0"

    implementation ("androidx.room:room-runtime:2.6.1")
    annotationProcessor ("androidx.room:room-compiler:2.6.1")

    implementation ("com.google.code.gson:gson:2.10.1")

    implementation ("androidx.cardview:cardview:1.0.0")
//Admob Ads
    implementation ("com.google.android.gms:play-services-ads:22.6.0")
//Recycleview
    implementation ("androidx.recyclerview:recyclerview:1.3.2")
//    kapt 'androidx.lifecycle:lifecycle-compiler:2.3.1'

    implementation ("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.7.2")

    // Kotlin
    implementation ("androidx.navigation:navigation-fragment-ktx:2.7.5")
    implementation ("androidx.navigation:navigation-ui-ktx:2.7.5")
    implementation ("androidx.navigation:navigation-ui-ktx:2.7.5")
    implementation ("com.google.android.material:material:1.11.0-alpha01")

//    implementation "io.coil-kt:coil:1.2.0"

    //LIFECYCLE COMPONENT
//    implementation "android.arch.lifecycle:extensions:1.0.0"
//    implementation 'com.irozon.sneaker:sneaker:2.0.0'
    /*
        implementation 'io.github.microutils:kotlin-logging:1.12.5'
        implementation 'net.alexandroid.utils:mylogkt:1.12'
        implementation 'com.github.anrwatchdog:anrwatchdog:1.4.0'
    */
    //fresco library
    implementation ("com.facebook.fresco:fresco:2.5.0")
    // annimation library
    implementation ("com.facebook.fresco:animated-webp:2.6.0")
    implementation ("com.facebook.fresco:webpsupport:2.6.0")

    /*PHOTO DRAWEE VIEW FOR ZOOM*/
    implementation ("me.relex:photodraweeview:2.1.0")
    //HTML
    implementation("com.jaredrummler:html-dsl:1.0.0")
}