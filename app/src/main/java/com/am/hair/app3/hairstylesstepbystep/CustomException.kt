package com.am.hair.app3.hairstylesstepbystep

class CustomException(message: String) : Exception(message)
